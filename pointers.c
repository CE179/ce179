#include <stdio.h>
int main()
{
    int x,y,*a,*b, temp;
    printf("enter the value of x and y\n");
    scanf("%d%d",&x,&y);
    printf("before swaping \nx=%d\n y=%d",x,y);
    a    = &x;
    b    = &y;
    temp = *b;
    *b   = *a;
    *a   = temp;
    printf("After swaping \nx=%d\n y=%d",x,y);
    return 0;
}